package cn.itcast.demo1;

public class TestStudent {
    public static void main(String[] args){
        Student stu = new Student();
        stu.setName("张三");
        stu.setAge(23);

        System.out.println(stu.getName());
        System.out.println(stu.getAge());
        System.out.println("---------------------");

        Student stu2 = new Student("李四",25);
        System.out.println(stu2.getName());
        System.out.println(stu2.getAge());

    }
}
