package cn.itcast.demo;

public class IfDemo3 {
    public static void main(String[] args){
        int time = 25;

        if(time >= 0 && time <= 12){
            System.out.println("小黑和我说: 上午好");
        }else if(time >=13 && time<=18){
            System.out.println("小黑和我说: 下午好");
        }else if(time >=19 && time<=24){
            System.out.println("小黑和我说: 晚上好");
        }else{
            System.out.println("小黑和我说:  我不认识这个时间!");
        }
    }
}
