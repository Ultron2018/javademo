package cn.itcast.demo;

import java.util.Scanner;
public class IfDemo4 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.println("请录入第一个整数: ");
        int a  = sc.nextInt();
        System.out.println("请录入第二个整数");
        int b= sc.nextInt();

        int max;

        if(a >=b){
            max =a;
        }else{
            max = b;
        }
        System.out.println("最大值是: "+ max);
    }
}
