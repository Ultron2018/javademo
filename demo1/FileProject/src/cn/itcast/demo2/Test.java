package cn.itcast.demo2;

import java.io.File;

public class Test {
    public static void main(String[] args){
        File file1 = new File("D:/java/demo1/FileProject/src/cn/itcast/abc/1.txt");
        String path1 = file1.getAbsolutePath();
        System.out.println("绝对路径: " + path1);

        String path2 = file1.getPath();
        System.out.println("相对路径: " + path2);
        System.out.println("===============================");

        File file2 = new File("D:\\java\\demo1\\FileProject\\src\\cn\\itcast/lib");
        String[] names  = file2.list();
        for(String name: names){
            System.out.println(name);
        }
        System.out.println("===============================");

        File[] files = file2.listFiles();
        for(File file:files){
            System.out.println(file);
        }



    }
}
