package cn.itcastdemo1;

public class Test {
    public static void main(String[] args){
        Employee em  = new Coder();
        em.show();
        System.out.println("------------------");

        final int NUM = 20;
        System.out.println(NUM);
        System.out.println("------------------");

        Employee em2 = new Employee();
        System.out.println("name属性值: " + em2.name);
        System.out.println("age属性值: " + em2.age);
    }
}
