package cn.itcast.demo;

public class OperationDemo4 {
    public static void main(String[] args){
        int a = 10;
        System.out.println("a: " + a );
        System.out.println("---------------------------------------");

        a += 20;
        System.out.println("a: " + a);
        System.out.println("-------------------------------------");

        short s = 2;
//        s = (short)(s + 1);
        System.out.println("s: " + s);
        System.out.println("------------------------------------");

        s += 3;
        System.out.println("s: " + s);


    }
}
