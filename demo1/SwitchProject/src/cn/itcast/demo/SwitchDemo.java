package cn.itcast.demo;

import java.util.Scanner;

public class SwitchDemo {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.println("请录入一个数字, 我来打印对应的日期");

        int week = sc.nextInt();

        switch(week){
            case 1:
                System.out.println("星期一");
                break;
            case 2:
                System.out.println("星期二");
                break;
            case 3:
                System.out.println("星期三");
                break;
            case 4:
                System.out.println("星球四");
                break;
            case 5:
                System.out.println("星期五");
                break;
            case 6:
                System.out.println("星期六");
                break;
            case 7:
                System.out.println("星期天");
                break;
            default:
                System.out.println("我不认识这样的时间");
        }
    }
}
