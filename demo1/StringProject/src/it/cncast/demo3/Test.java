package it.cncast.demo3;

public class Test {
    public static void main(String[] args){
        String s1 = "abc";

        byte[] bys = s1.getBytes();
        for(int i=0;i<bys.length;i++){
            System.out.println(bys[i]);
        }

        System.out.println("--------------------------");

        char[] chs =s1.toCharArray();
        for(int i=0;i<chs.length;i++){
            System.out.println(chs[i]);
        }

        System.out.println("--------------------------");
        String s2 = String.valueOf(123);
        System.out.println(s2 + 4);

        String s3 = ""+123;
        System.out.println(s3+4);
        System.out.println("--------------------------");

        String s4 = "abc abc abc";
        String s5 = s4.replace('b','d');
        System.out.println("s5: "+s5);
        System.out.println("--------------------------");


        String[] arr = s4.split(" ");
        for (int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
        System.out.println("--------------------------");

        String s6 = "   a  b   c   ";
        String s7 = s6.trim();
        System.out.println(s6);
        System.out.println(s7);
        System.out.println("--------------------------");

















    }
}
