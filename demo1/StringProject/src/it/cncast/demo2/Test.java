package it.cncast.demo2;

public class Test {
    public static void main(String[] args){
        String str = "java 程序员 java";
        int length =str.length();
        System.out.println(length);
        System.out.println(str.length());

        char ch = str.charAt(1);
        System.out.println(ch);
        System.out.println("------------------------");

        int index1 = str.indexOf('a');
        System.out.println("index1: " + index1);

        int index2 = str.lastIndexOf('a');
        System.out.println("index2: "+ index2);

        String s1 = str.substring(5);
        System.out.println("s1: "+ s1);

        String s2 = str.substring(5,10);
        System.out.println("s2: "+ s2);

    }
}
