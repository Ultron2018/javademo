package it.cncast.demo1;

public class Test {
    public static void main(String[] args){
        byte[] bys = {97,98,99};
        String s1 = new String(bys);
        System.out.println("s1: " + s1);
        System.out.println("-----------------------");

        char[] chs = {'h','e','l','l','o'};
        String s2 = new String(chs);
        System.out.println("s2: " + s2);

        String s3 = "abc";
        System.out.println("------------------");

        String str1 = "abc";
        String str2 = "ABC";
        boolean b1 = str1.equals(str2);
        System.out.println("equals(): "+ b1);

        boolean b2 = str1.equalsIgnoreCase(str2);
        System.out.println("equalsIgnoreCase():" + b2);

        boolean b3 = str1.startsWith("ab");
        System.out.println("startsWith: "+ b3);


        String str3 = "hello";
        boolean b4 = str3.isEmpty();
        System.out.println("isEmpty(): "+ b4);


    }
}
