package cn.itcast.demo1;

import java.util.Calendar;

public class Test2 {
    public static void main(String[] args){
        Calendar c = Calendar.getInstance();
        System.out.println(c);

        int year  = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DATE);
        System.out.println(year + "年"+(month+1)+"月"+day+"日");
        System.out.println("--------------------");

        c.set(Calendar.YEAR,2030);
        int year2 = c.get(Calendar.YEAR);
        System.out.println(year2 + "年" +(month+1)+"月"+day+"日");
        System.out.println("--------------------");


        c.set(2030,5,1);
        int year1 = c.get(Calendar.YEAR);
        int month1 = c.get(Calendar.MONTH);
        int day1 = c.get(Calendar.DATE);
        System.out.println(year1 + "年"+(month1+1)+"月"+day1+"日");


    }
}
