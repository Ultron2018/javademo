package cn.itcast.demo1;

import java.util.Date;

public class Test {
    public static void main(String[] args){
        Date date1 = new Date();
        System.out.println("date1: "+date1);

        long time = date1.getTime();
        System.out.println("time: " + time);

        Date date2 = new Date(1614234717140L);
        System.out.println("date2: "+ date2);
    }
}
