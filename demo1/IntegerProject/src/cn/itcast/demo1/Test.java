package cn.itcast.demo1;

public class Test {

    public static void main(String[] args){
        int a = 10;
        Integer i1= new Integer(20);
        int b= i1.intValue();
        System.out.println(i1);
        System.out.println(b);
        System.out.println("-------------------------");

        Integer i2 = 30;
        int c =i2;
        System.out.println("----------------------");

        String s= "10";
        int num = Integer.parseInt(s);
        System.out.println("num: "+ num);
        System.out.println("num + 100 = " + (num+100));


    }
}
