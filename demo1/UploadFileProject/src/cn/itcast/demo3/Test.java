package cn.itcast.demo3;

import java.lang.reflect.Method;

public class Test {
    public static void main(String[] args){
        String s ="hello";
        Test.printClassMethodMessage(s);

        Integer n1 =  1;
        Test.printClassMethodMessage(n1);
    }

    public static void printClassMethodMessage(Object obj){
        Class c = obj.getClass();
        //获取类名称
        System.out.println("类的名称是: "+ c.getName());

        Method[] ms = c.getMethods();
        for(int i=0;i<ms.length;i++){
            Class retturnType = ms[i].getReturnType();
            System.out.println(retturnType.getName()+ " ");

            //得到方法名
            System.out.println(ms[i].getName()+"(");
            //获取参数类型--->得到的是参数列表的的类型
            Class[] paramTypes = ms[i].getParameterTypes();
            for(Class class1:paramTypes){
                System.out.println(class1.getName()+",");
            }
            System.out.println(")");
        }
    }
}
