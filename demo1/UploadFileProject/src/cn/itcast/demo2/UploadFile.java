package cn.itcast.demo2;

import java.io.File;
import java.util.Scanner;

public class UploadFile {
    public static void main(String[] args){
        File path = getPath();
        System.out.println(path);
        boolean flag = isExists(path.getPath());
        if(flag){
            System.out.println("该用户图像已经存在,上传失败");
        }else{
            System.out.println("我马上要上传了,代码稍后写");
        }
    }

    public static File getPath(){
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入您要上传的用户图像路径");
        String path = sc.nextLine();

        if(!path.endsWith(".jpg") && !path.endsWith(".png") && !path.endsWith("bmp")){
            System.out.println("你输入的不是图片,请重新录入");
        }

        File file = new File(path);
        if(file.exists() && file.isFile()){
            return file;
        }else{
            System.out.println("您输入的图片路径不合法,请重新录入");
        }
        return file;
    }

    public static boolean isExists(String path){
        File file = new File("lib");
        String[] names = file.list();
        for (String name: names){
            if(name.equals(path)){
                return true;
            }
        }
        return false;
    }

}
