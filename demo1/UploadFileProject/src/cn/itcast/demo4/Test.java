package cn.itcast.demo4;

import java.lang.reflect.Method;

public class Test {
    public static void main(String[] args){
        Class<?> herosClass = Heros.class;
        Method[] methods= herosClass.getMethods();
//        for(Method method :methods){
//            System.out.println(method);
//        }
        try{
            Method m1 = herosClass.getMethod("setName",String.class);
            Method m2 = herosClass.getMethod("getName");
            Object userInfo = herosClass.newInstance();
            m1.invoke(userInfo,"影魔");
            System.out.println("调用set方法: "+ userInfo);
            System.out.println("调用get方法: "+ m2.invoke(userInfo));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
