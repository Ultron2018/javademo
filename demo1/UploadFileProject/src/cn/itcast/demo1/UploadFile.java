package cn.itcast.demo1;

import java.io.File;
import java.io.Serializable;
import java.net.http.HttpRequest;
import java.util.Scanner;

public class UploadFile {
    public static void main(String[] args) {
        System.out.println(UploadFile.getPath());
    }

    public static Serializable getPath() {
        Scanner sc = new Scanner(System.in);
        System.out.println("请录入您要上传的用户图像的路径");
        String path = sc.nextLine();
        if (!path.endsWith(".jpg") && !path.endsWith(".png") && !path.endsWith("bmp")) {
            return "D:/后台模板/X-admin-v2.0/X-admin/images/bg.png";
        } else {
            File file = new File(path);
            if (!file.isFile() && !file.exists()) {
                System.out.println("图片路径不合法");
            }
            return file;
        }
    }
}
