package cn.itcast.demo1;

public class Test {
    public static void main(String[] args){
        Dog dog = new Dog();
        dog.eat();

        Mouse mouse = new Mouse();
        mouse.eat();
        System.out.println("------------");

        Animal an = new Dog();
        an.eat();

    }
}
