package cn.itcast.demo3;

public class Coder extends Employee{
    public Coder() {
        super();
    }

    public Coder(String name, double salary, String id) {
        super(name, salary, id);
    }

    @Override
    public void work(){
        System.out.println("程序员要敲代码");
    }
}
