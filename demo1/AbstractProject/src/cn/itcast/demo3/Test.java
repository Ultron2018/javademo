package cn.itcast.demo3;

public class Test {
    public static void main(String[] args){
        Employee em = new Coder();
        em.work();

        Employee em2 = new Manager();
        em2.work();
        System.out.println("--------------------");

//        Coder c =  new Coder();
//        c.setName("张三");
//        c.setSalary(30000);
//        c.setId("研发部007");

        //使用构造器
        Coder c = new Coder("李四",3000,"研发部006");
        System.out.println("姓名: "+c.getName());
        System.out.println("工资: "+c.getSalary());
        System.out.println("工号: " + c.getId());
        System.out.println("----------------------------------");

        //
        Manager m = new Manager("悠悠",40000,"研发部004",10000);





    }
}
