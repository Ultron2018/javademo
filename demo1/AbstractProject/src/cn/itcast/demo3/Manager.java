package cn.itcast.demo3;

public class Manager extends Employee {
   public Manager(){
       super();
   }
   public Manager(String name,double salary,String id,int bonus){
        super(name,salary,id);
        this.bonus = bonus;
   }


    private int bonus;
    public int getBonus(){
        return bonus;
    }

    @Override
    public void work(){
        System.out.println("经理喝着茶翘着二郎腿看着程序员写代码");
    }
}
