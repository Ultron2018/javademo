public class VariableDemo2 {
    public static void main(String[] args){
        byte b =10;
        System.out.println(b);


        short s = 20;
        System.out.println(s);

        int i = 30;
        System.out.println(i);

        long l = 1000000000000L;
        System.out.println(l);


        float f = 10.3F;
        System.out.println(f);

        double d = 10.2;
        System.out.println(d);

        char c ='a';
        System.out.println(c);

        boolean b1 = true;
        boolean b2 = false;
        System.out.println(b1);
        System.out.println(b2);
        System.out.println("----------------------------------");


//        int a = 10;
        int a;
        a=10;
        System.out.println(a);


        {
            int aa=20;
            System.out.println(aa);
        }


        int bb=1, cc=2,dd=3,ee=4;
        System.out.println(bb);
        System.out.println(cc);
        System.out.println(dd);
        System.out.println(ee);



    }
}
