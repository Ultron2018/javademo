package cn.itcast.demo4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Test {
    public static void main(String[] args){
        List list  = new ArrayList();
        list.add("a");
        list.add("b");
        list.add("c");

//        Iterator it = list.iterator();
//        while(it.hasNext()){
//            String s = (String)it.next();
//            System.out.println(s);
//        }

        System.out.println("----------------------------");
        Iterator it = list.listIterator();

        while(it.hasNext()){
            String s = (String)it.next();
            if("b".equals(s)){
                list.add("java");
            }
            System.out.println(s);
        }




    }
}
