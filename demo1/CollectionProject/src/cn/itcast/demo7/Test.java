package cn.itcast.demo7;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Test {
    public static void main(String[] args){
        Set<Student> set = new HashSet<>();
        Student s1 = new Student("巧粉",20);
        Student s2 = new Student("虚竹",30);
        Student s3 = new Student("段誉",24);
        Student s4 = new Student("晓峰",26);
        Student s5 = new Student("虚竹",30);

        set.add(s1);
        set.add(s2);
        set.add(s3);
        set.add(s4);
        set.add(s5);

        System.out.println(set);
        System.out.println("--------------------");

        Iterator<Student> it = set.iterator();
        while(it.hasNext()){
            Student s = it.next();
            System.out.println(s);
        }
        System.out.println("--------------------");

        for(Student student:set){
            System.out.println(student);
        }

    }
}
