package cn.itcast.demo2;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args){
        List list  = new ArrayList();
        Student s1 = new Student("乔大哥",40);
        Student s2 = new Student("晓峰",20);
        Student s3 = new Student("柱子",30);
        Student s4 = new Student("鱼子",20);

        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);

        System.out.println(list);
        Object obj = list.get(2);
        System.out.println("索引为2的元素是:"+ obj);
        System.out.println("集合长度为: "+ list.size());
        System.out.println("=========================");

        for(int i=0;i<list.size();i++){
            Object obj2 = list.get(i);
            System.out.println("索引为: "+ i + "的元素是: "+ obj2 );
        }




    }
}
