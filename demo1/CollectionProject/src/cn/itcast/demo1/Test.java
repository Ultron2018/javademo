package cn.itcast.demo1;


import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args){
        List list = new ArrayList();
        Student s1 = new Student("乔峰",41);
        Student s2 = new Student("晓峰",32);
        Student s3 = new Student("虚竹",31);
        Student s4 = new Student("段誉",28);

//        boolean b1 = list.add(s1);
//        System.out.println(b1);
//        boolean b2 = list.add(s2);
//        System.out.println(b2);
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);
        System.out.println(list);
        Object obj = list.get(2);
        System.out.println("索引为2的元素是: "+ obj);
        System.out.println("集合长度为: "+ list.size());
        System.out.println("---------------------------");

        for(int i=0;i<list.size();i++){
            Object obj2 = list.get(i);
            System.out.println("索引为 " + i + "的元素是: " + obj2);
        }

    }
}
