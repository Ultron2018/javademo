package cn.itcast.demo6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test {
    public static void main(String[] args){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);

        System.out.println("没有操作以前,集合数据是: "+ list);
        System.out.println("----------------------------");

        Integer max = Collections.max(list);
        System.out.println("集合中的最大元素为: "+ max);
        System.out.println("---------------------");

//        Collections.sort(list);
//        System.out.println("升序排列后的结果为:" + list);
//        System.out.println("---------------------");
//
//        Collections.reverse(list);
//        System.out.println("反转以后集合中的数据为: "+ list);
        System.out.println("---------------------");

        Collections.shuffle(list);
        System.out.println("随机置换后的结果为: "+list);



    }
}
