package cn.itcast.demo1;

public class Test {
    public static void main(String[] args){
        Object obj1 = new Object();
        Object obj2 = new Object();

        int code1 = obj1.hashCode();
        int code2 = obj2.hashCode();
        System.out.println(code1);
        System.out.println(code2);
        System.out.println("-------------------------");

        Class classz1 = obj1.getClass();
        Class classz2 = obj2.getClass();
        System.out.println(classz1);
        System.out.println(classz2);
        System.out.println("-------------------------");

        String s1 = obj1.toString();
        String s2 = obj2.toString();
        System.out.println(s1);
        System.out.println(s2);
        System.out.println("-------------------------");

        boolean b1 = obj1.equals(obj2);
        System.out.println(b1);


    }
}
