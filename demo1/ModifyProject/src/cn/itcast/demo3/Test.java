package cn.itcast.demo3;

public class Test {
    public static void main(String[] args){
//        Animal an = new Animal();
        Animal an = new Dog();
        an.eat();

//        Dog dog = (Dog)an;
//        dog.watch();

//        Cat c = (Cat)an;

            if(an instanceof Dog){
                Dog dog = (Dog)an;
                dog.watch();
            }

    }
}
