package it.cncast.demo1;

public class Test {
    public static void main(String[] args){
        Developer dl = new Developer();
        dl.name="小黑";
        dl.work="写代码";
        dl.departmentName = "开发部";
        dl.selfIntroduction();

        Developer d2 = new Developer();
        d2.name = "媛媛";
        d2.work = "鼓励师";
//        d2.departmentName = "研发部";
        d2.selfIntroduction();
    }
}
