package cn.itcast.eshop.user;

import cn.itcast.eshop.common.action.BaseAction;

/**
 * 用户控制器类
 * 处理所有用户后台操作,并返回JSON信息
 */
public class UserAction extends BaseAction {
    private String username;
    private String password;

    /***
     * 用户登录功能
     * @return
     */
    public String login(){
        return "";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
