package cn.itcast.eshop.common.entity;

/**
 * 实体类
 * 所有模块实体父类
 * 职责:封装数据
 */
public class Entity {
    /*数据唯一标识*/
    private String id ;
    /*创建时间*/
    private String crateTime;
    /*删除时间*/
    private String deleteTime;
    /*删除状态 0以删除, 1正常 默认值1*/
    private String idDel = "1";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCrateTime() {
        return crateTime;
    }

    public void setCrateTime(String crateTime) {
        this.crateTime = crateTime;
    }

    public String getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(String deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getIdDel() {
        return idDel;
    }

    public void setIdDel(String idDel) {
        this.idDel = idDel;
    }
}
