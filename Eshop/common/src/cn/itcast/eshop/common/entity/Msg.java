package cn.itcast.eshop.common.entity;

/***
 * 消息封装类
 */
public class Msg {
    /*** 消息类型:成功* */
    public static final String SUCCESS = "SUCCESS";
    /**消息类型:失败*/
    public static final String FAIL = "FAIL";

    /**类型*/
    private String type;
    /**内容*/
    private String msg;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
