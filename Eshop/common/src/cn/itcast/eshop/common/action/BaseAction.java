package cn.itcast.eshop.common.action;

/**
 * Action, 控制器类的基类
 * 1.封装请求数据
 * 2.校验权限
 * 3.调用服务层(Service)处理业务逻辑
 * 4.日志记录
 * 5.返回消息到客户端
 */
public class BaseAction {
}
