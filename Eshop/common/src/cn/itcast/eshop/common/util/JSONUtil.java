package cn.itcast.eshop.common.util;

import cn.itcast.eshop.common.entity.Entity;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * JSON工具类
 * 处理和JSON相关的所有内容
 * */
public class JSONUtil {
    public static void main(String[] args){
        Entity entity = new Entity();
        entity.setId("100");
        entity.setCrateTime("18:16");
        String json = entity2JSON(entity);
        System.out.println(json);       //{"crateTime":"18:16","id":"100","idDel":"1"}

        System.out.println("----------------------");
        List<Entity> entityList = new ArrayList<>();
        entityList.add(entity);
        String jsonList = entityList2JSON(entityList);
        System.out.println(jsonList);       //[{"crateTime":"18:16","id":"100","idDel":"1"}]


        System.out.println("----------------------");
        String jsonStr = "{\"crateTime\":\"18:16\",\"id\":\"100\",\"idDel\":\"1\"}";
        Object obj = JSON2Entity(jsonStr,Entity.class);
        Entity e = (Entity) obj;
        System.out.println(e.getCrateTime());

        System.out.println("----------------------");
        String jsonArray = "[{\"crateTime\":\"18:30\",\"id\":\"100\",\"idDel\":\"1\"},{\"crateTime\":\"18:16\",\"id\":\"100\",\"idDel\":\"1\"}]";
        List<Entity> e1 = JSONArray2List(jsonArray,Entity.class);
        System.out.println(e1.get(0).getCrateTime());


    }




    /**
     *把对象转换成JSON格式字符串
     * @param entity  指定对象
     * @return  返回JSON格式的字符串
     */
    public static String entity2JSON(Object entity){
        return JSON.toJSONString(entity);
    }

    /***
     * 把对象列表转成JSON格式的字符串
     * @param entityList 指定对象列表
     * @return  返回JSON格式的字符串
     */
    public static String entityList2JSON(List<?> entityList){
        return entity2JSON(entityList);
    }

    /**
     * 把JSON字符串转换成指定类型的对象
     * @param json  要转换的数据
     * @param classzz   指定的类型
     * @return  返回Object对象
     */
//    public static Object JSON2Entity(String json, Class<?> classzz){
//        Object obj = JSON.parseObject(json,classzz);
//        return obj;
//    }

    public static <T> T JSON2Entity(String json,Class<T> classzz){
        return JSON.parseObject(json, classzz);
    }


    /**
     * 把JSON数组转换成指定类型的列表
     * @param json  数据
     * @param classzz 指定的类型Class对象
     * @param <T> 指定类型
     * @return 对象列表
     */
    public static <T> List<T> JSONArray2List(String json, Class<T> classzz){
        return JSON.parseArray(json,classzz);
    }


}
