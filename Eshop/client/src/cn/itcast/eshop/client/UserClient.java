package cn.itcast.eshop.client;

import cn.itcast.eshop.common.entity.Msg;
import cn.itcast.eshop.common.util.JSONUtil;
import cn.itcast.eshop.user.UserAction;

import java.util.Scanner;

/**
 * 用户操作界面
 * 所有用户相关操作内容
 */
public class UserClient extends Client {
    public String showLogin(){
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入用户名: ");
        String username = sc.nextLine();
        System.out.println("请输入密码: ");
        String password = sc.nextLine();

       UserAction userAction = new UserAction();
       userAction.setUsername(username);
       userAction.setPassword(password);

       String result =  userAction.login();

        JSONUtil.JSON2Entity(result, Msg.class);


    }
}
